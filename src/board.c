#include "board.h"

Board * newBoard(int columns, int rows)
{
    int x,y;

    // Allocate and initialize a new board.
    Board * board = (Board *) malloc(sizeof(Board));
    
    board->cols = columns;
    board->rows = rows;
    
    board->heights = (int *) malloc(columns * sizeof(int));
    board->grid = (Cell ***) malloc(columns * sizeof(Cell **));
    
    // Create a new Cell in each possition of the grid and
    // initialize the heights to 0.
    for(x = 0; x < columns; x++)
    {
        board->grid[x] = (Cell **) malloc(rows * sizeof(Cell *));
        board->heights[x] = 0;
        for(y = 0; y < rows; y++) board->grid[x][y] = newCell();
    }

    return board;
}

void deleteBoard(Board* board)
{
    // Delete all Cells inside the board
    int x,y;
    for(x=0; x < board->cols; x++){
        for(y=0; y < board->rows; y++){
            deleteCell(board->grid[x][y]);
        }
        free(board->grid[x]);
    }

    // Free data sructures
    free(board->grid);
    free(board->heights);
    free(board);
}

int getRows(Board* board)
{
    return board->rows;
}

int getCols(Board* board)
{
    return board->cols;
}

int getCells(Board* board)
{
    return (board->rows * board->cols); 
}

Cell* getCell(Board* board, int col, int row)
{
    return board->grid[col][row]; 
}

int freeSpaceInColumn(Board* board, int col)
{
    return board->heights[col] < board->cols - 1;
}

void addPiece(Board * board, int column, state_t player)
{
    // Modify the cell state and increase column height
    // WARNING: Instruction order is important !
    int row = board->heights[column];
    setState(board->grid[column][row], player);
    board->heights[column]++;
}

void removePiece(Board * board, int column)
{
    // Modify the cell state and decrease column height
    // WARNING: Instruction order is important !
    board->heights[column]--;
    int row = board->heights[column];
    setState(board->grid[column][row], EMPTY);
}

char ** boardtoArrayRepresentation(Board * board)
{
    int c,r;
    char **rep = (char **) malloc(board->rows * sizeof(char *));

    for(r=0; r < board->rows; r++){
        rep[r] = (char *) malloc(board->cols * sizeof(char));
        for(c=0; c < board->cols; c++){
            if (getState(board->grid[c][r]) == EMPTY) rep[r][c] = 0;
            if (getState(board->grid[c][r]) == PLAYER_ONE) rep[r][c] = 1;
            if (getState(board->grid[c][r]) == PLAYER_TWO) rep[r][c] = 2;
        }
    }
    return rep;
}


