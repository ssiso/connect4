#include "aiStrategy.h"

// Private method headers
int maxValue(Game * game, int depth_left);
int minValue(Game * game, int depth_left);
int getStrength(Game * game);



int getReasonedMove1(Game * game){
    int moves[game->board->cols];
    int highest = 0;
    int i;
    int depth = 4;

    // Get which is the best column move
    for( i = 0; i < game->board->cols; i++) {
        moves[i] = INT_MIN;
        if(validMove(game, i))
        {
            makeMove(game,i);
            moves[i] = minValue(game, depth);
            if(moves[i] >= moves[highest]) highest = i;
            undoMove(game); //restore state
        } 
    }
    return highest;
}

// AI Stragegy private method
// Returns the sum of an heuristic computed for each line
// The heuristic returns 0,1,10,50,600 depending on the 
// number of pieces a player has on the line (line->score)
int getStrength(Game * game){
    int sum=0;
    int weights[] = {0,1,10,50,600};
    int i;

    // Sum the weight that each line score maps to
    // Player 1 adds possitive weights
    // Player 2 adds negative weigths
    for( i=0; i < game->num_lines; i++)
    {
        if (game->all_lines[i]->score > 0){
           sum += weights[abs(game->all_lines[i]->score)];
        }else{
           sum -= weights[abs(game->all_lines[i]->score)];
        
        }
    }

    // Add or substract 16 if player has the next move
    return sum + (game->turn == PLAYER_ONE ? 16 : -16);
}

// AI Strategy private method
// Minimizing part of the minmax algorithm
// depth_left tells the number of recursive steps left to check
int minValue(Game * game, int depth_left)
{
    int moves[game->board->cols];
    int lowest = 0;
    int i;

    // If it is a terminal node return value
    if(winnerIs(game) != 0 || depth_left == 0) return -getStrength(game);

    // Get which is the best column move
    for( i=0; i < game->board->cols; i++) {
        moves[i] = INT_MAX;
        if(validMove(game,i))
        {
            makeMove(game,i);
            moves[i] = maxValue(game, depth_left - 1);
            if(moves[i] < moves[lowest]) lowest = i;
            undoMove(game);
        }
    }

    return moves[lowest];
}

// AI Strategy private method
// Maximizing part of the minmax algorithm
// depth_left tells the number of recursive steps left to check
int maxValue(Game * game, int depth_left){
    int moves[game->board->cols];
    int highest = 0;
    int i;

    // If it is a terminal node return value
    if(winnerIs(game) != 0 || depth_left == 0) return -getStrength(game);

    // Get which is the best column move
    for( i=0; i< game->board->cols; i++){
        moves[i] = INT_MIN;
        if(validMove(game,i))
        {
            makeMove(game,i);
            moves[i] = minValue(game, depth_left - 1);
            if(moves[i] < moves[highest]) highest = i;
            undoMove(game);
        }
    }

    return moves[highest];
}
