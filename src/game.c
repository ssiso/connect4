#include "game.h"
#include "aiStrategy.h"

//Private functions
void changePlayer(Game * game);
Line ** generateLines(Cell *** grid, int columns, int rows, int numlines);

Game * newGame(int columns, int rows, int starting_player, int (*ai)(Game *))
{
    // Check input preconditions, return NULL if unacceptable
    if(columns < 4 || rows < 4 ) return NULL;
    if(!(starting_player == 1 || starting_player == 2)) return NULL;

    // Create and initialize Game object
    Game * game = (Game *) malloc(sizeof(Game));
  
    game->move_counter = 0;
    if (starting_player == 1) game->turn = PLAYER_ONE;
    if (starting_player == 2) game->turn = PLAYER_TWO;
    game->board = newBoard(columns,rows);
    game->moves = (int *) malloc(columns * rows * sizeof(int));

    // To get the number of possible connect4 lines we add the
    // rows * cols possibilites for each direction, substracting
    // 3 to the direction they will 
    game->num_lines = 0;
    game->num_lines += (rows - 3) * columns; //horitzontal
    game->num_lines += (columns - 3) * rows; //vertical
    game->num_lines += (columns - 3) * (rows - 3) * 2; //diagonal

    game->all_lines = generateLines(game->board->grid, columns,
                                    rows, game->num_lines);

    game->strategy = ai;

    return game;
}

void deleteGame(Game * game)
{
    int i;
    for(i=0; i < game->num_lines; i++) free(game->all_lines[i]);
    free(game->all_lines);
    deleteBoard(game->board);
    free(game->moves);
    free(game);
}

/**
 *  This Game private function add to all the cells of the board
 *  the references to the lines they belong.
 */
Line ** generateLines(Cell *** grid, int columns, int rows, int numlines)
{
    int i, y, x, t;
    
    //Create and array with all the possible 4 cell lines.
    Line ** lines = (Line **)malloc(numlines * sizeof(Line *));
    for(i = 0; i < numlines; i++){
        lines[i] = newLine(0);
    }

    //Add cells to Horitzontal Lines
    int count =0;
    for(y = 0; y < rows; y++){
	    for( x = 0; x < columns - 3; x++){
	        for( i = x; i< x+4 ; i++){ 
                addLine(grid[i][y], lines[count]);
            }
            count++;
        }
    }

    //Add cells to Vertical Lines
    for( x = 0; x < columns; x++){
        for( y = 0; y < rows - 3; y++){
            for( i = y; i < y+4; i++){
                addLine(grid[x][i], lines[count]);
            }
            count++;
        }
    }

    //Add cells to Diagonal Lines leftdown <-> rightup
    for( x = 0; x < columns - 3; x++){
        for( y = 0; y < rows - 3; y++){
            for( t = x, i = y; t < x+4 && i < y+4; t++, i++){
                addLine(grid[t][i], lines[count]);
            }
            count++;
        }
    }

    //Add cells to Diagonal Lines leftup <-> rightdown
    for( x = 0; x < columns - 3; x++){
        for( y = rows - 1; y > 2; y--){
            for( t = x, i = y; t < x+4 && i > -1; t++, i--){
                addLine(grid[t][i], lines[count]);
            }
            count++;
        }
    }

    return lines;
}

int validMove(Game * game, int column)
{
	return freeSpaceInColumn(game->board, column);
}

void makeMove(Game * game, int column)
{
    // Add the piece, add a move and change the player.
    addPiece(game->board, column, game->turn);
    game->move_counter++;
    game->moves[game->move_counter] = column;
    changePlayer(game);
}

void changePlayer(Game * game)
{
    //Swap the turn variable from PLAYER_ONE <-> PLAYER_TWO.
    if(game->turn == PLAYER_ONE) game->turn = PLAYER_TWO;
    else if(game->turn == PLAYER_TWO) game->turn = PLAYER_ONE;
}


void undoMove(Game * game)
{
    //Remove the piece, decrease moves and change player.
    int column = game->moves[game->move_counter];
    removePiece(game->board, column);
    game->move_counter--;
    changePlayer(game);
}

int validMovesLeft(Game * game)
{
    // Moves left are the total number of cells - the moves already done.
    return getCells(game->board) - game->move_counter;
}

int winnerIs(Game * game)
{
   int i;
    for( i = 0; i < game->num_lines; i++)
    {
        if(game->all_lines[i]->score == WIN_ONE) return 1;
        else if(game->all_lines[i]->score == WIN_TWO) return 2;
    }
    return 0;
}

int turn(Game * game)
{
    if(game->turn == PLAYER_ONE) return 1;
    else return 2;
}


char ** toArrayRepresentation(Game * game){
    return boardtoArrayRepresentation(game->board);
}

int getReasonedMove(Game * game){
    return game->strategy(game);
}
