#include "line.h"


Line * newLine(int initialscore)
{
	Line * line = (Line *) malloc(sizeof(Line));
	line->score = initialscore;
	return line;
}

void deleteLine(Line * line)
{
	free(line);
}

int getLineScore(Line * line)
{
    return line->score;
}
