#include <stdio.h>
#include <string.h>
#include "game.h"
#include "aiStrategy.h"

#define SIZE 10
/*
 *  Converts the array[rows][cols] that represents the board
 *  to a printable string.
 */
char* arrayToString(char ** barray, int rows, int cols);


/*
 *   Program entry point and implementation of the CLI user interface.
 */
int main(int argc, char** argv) {

    /**
     *  Argument checking and parsing
     */
    if(argc != 1 && argc != 2){
        printf("ERROR: Invalid number of arguments!\n");
        exit(-1);
    }
    if (argc == 2){
        if(strcmp(argv[1], "-help") == 0){
            printf("\nConnect 4 is a two-player connection game in which players take turns dropping ");
            printf("a colored discs from top into a seven-column, six-row vertically suspended grid. ");
            printf("The object of the game is to connect four of one's own discs of the same colour ");
            printf("before your oppenent. (vertical, horizontal or diagonal connections)\n");
            printf("\nGame controls:\n");
            printf(" Ctrl+c : Exits the game. \n");
            printf(" Number : Selects the column where your next move is done. \n");
            printf("\nFlags:\n");
            printf(" -help : Display the help information\n");

            exit(0);
        }else{
            printf("ERROR: unknown argument %s\n",argv[1]);
            exit(-2);
        }
    }
    
    /*
     *  Initialize a new game with 7 columns 6 rows and
     *  Player 1 starts moving.
     */

    int columns = 7;
    int rows = 6;
    int human_player = 1;
    Game * game = newGame(columns, rows, human_player, &getReasonedMove1);
    if(game == NULL){
        printf("ERROR: The game could not be initialized\n");
        exit(-1);
    }
    printf(" Started new game with %d columns and %d rows\n\n", columns, rows);
    printf("%s\n", arrayToString(toArrayRepresentation(game), rows, columns));


    /*
     *   Game loop (iterate until game finish)
     *   When is player 1 turn get movement from stdin
     *   when is player 2 turn make reasoned move
     */ 
    char input[SIZE];
    int move;
    while((winnerIs(game) == 0) && validMovesLeft(game) > 0)
    {
        if(turn(game) == 1)	
        {
            printf("It is your turn, choose an empty column from 1 to %d: ", columns);
            move = 0;
            fgets(input,SIZE,stdin);
            move = atoi(input);

            while (move < 1 || move > columns || !validMove(game, move - 1)){
              printf("ERROR: choose an empty column form 1 to %d: ", columns);
              fgets(input,SIZE,stdin);
              move = atoi(input);
            }
            makeMove(game, move - 1); // Translate move to 0-indexing.
        }else{
            printf("Wait, it is computer turn!\n");
            makeMove(game, getReasonedMove(game));
        }
        printf("%s\n", arrayToString(toArrayRepresentation(game), rows, columns));

    }

    /**
     *  Output the result of the game and quit the program.
     */
    if(winnerIs(game) == 1){
        printf("\nCongratulations, you WIN the game!\n\n");
    }else if(winnerIs(game) == 2){
        printf("\nCOMPUTER wins the game!\n\n");
    }else{
        printf("\nAll cells used. There is no winner!\n\n");
    }

    //deleteGame(game);
    return 0;
}


char* arrayToString(char ** barray, int rows, int cols){
    
    char * temp = (char *)malloc(rows*(cols+1)*sizeof(char)+1);
    char * curr = temp;
    int y;
    int x;

    for(y = rows-1; y > -1; y--){
        for(x=0; x < cols; x++){
            if(barray[y][x] == 0) *curr = '-';
            else if(barray[y][x] == 1) *curr = 'O';
            else if(barray[y][x] == 2) *curr = 'X';
            curr++;
        }
        *curr = '\n';
        curr++;
    }
    return temp;
}
