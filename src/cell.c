#include "cell.h"
#include <stdio.h>

Cell* newCell()
{
    Cell * cell = (Cell*) malloc(sizeof(Cell));
    cell->state = EMPTY;
    cell->lines = newLinkedList();
    return cell;
}

void deleteCell(Cell* cell)
{
    deleteLinkedList(cell->lines);
    free(cell);
}

void addLine(Cell* cell, Line * line){
    addItem(cell->lines,line);
}

void setState(Cell* cell, state_t player)
{
    int score_modifier;
    if(cell->state == EMPTY && player == PLAYER_ONE) score_modifier = +1;
    if(cell->state == EMPTY && player == PLAYER_TWO) score_modifier = -1;
    if(cell->state == PLAYER_ONE && player == EMPTY) score_modifier = -1;
    if(cell->state == PLAYER_TWO && player == EMPTY) score_modifier = +1;

    //Not valid options, abort if detected
    if(cell->state == PLAYER_ONE && player == PLAYER_TWO) exit(-2);
    if(cell->state == PLAYER_TWO && player == PLAYER_ONE) exit(-2);
    if(cell->state ==  player ) exit(-2);

    
    linkedList * ll = cell->lines;
    while(!lastItem(ll)){
        ll->item->score += score_modifier;
        ll = ll->next;
    }

    cell->state = player;
}

state_t getState(Cell* cell)
{
    return cell->state;
}

