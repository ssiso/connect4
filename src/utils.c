#include <utils.h>


linkedList* newLinkedList()
{
    linkedList * ll = (linkedList*) malloc(sizeof(linkedList));
    ll->item = NULL;
    ll->next = NULL; 
    return ll;
}

void deleteLinkedList(linkedList * ll)
{
    // Recursivelly delete all nodes of the list
    if(!lastItem(ll)) deleteLinkedList(ll->next);
    free(ll);
}

void addItem(linkedList * ll, Line * item)
{
    // Go to the last item of the list
    linkedList * last = ll;
    while(!lastItem(last)){
        last = last->next;
    }

    // Allocate the new node and save the proper references
    linkedList * newi = (linkedList*) malloc(sizeof(linkedList));
    newi->item = NULL;
    newi->next = NULL;
    last->item = item;
    last->next = newi;
}

int lastItem(linkedList * ll)
{
    // If it is the ghost node, it is the last element
    if(ll->item == NULL && ll->next == NULL) return 1;
    else return 0;
}

linkedList * nextItem(linkedList * ll)
{
    return ll->next;
}
