

CC = gcc
CFLAGS = -g -Wall
LDFLAGS =

MAIN = src/main.c
SRCS = src/utils.c src/board.c src/cell.c src/game.c src/aiStrategy.c src/line.c
OBJS = $(patsubst src/%.c, bin/%.o, $(SRCS))
HEADERS = $(patsubst src/%.c, include/%.h, $(SRCS))

TESTMAIN = test/src/mainTest.c
TESTSRC = test/src/utilsTest.c test/src/boardTest.c \
		  test/src/cellTest.c test/src/gameTest.c test/src/linesTest.c
TESTOBJ = $(patsubst test/src/%.c, bin/test/%.o, $(TESTSRC))


all: executable

bin/%.o: src/%.c Makefile
	$(CC) $(CFLAGS) -I include $< -c -o $@

bin/test/%.o: test/src/%.c Makefile
	$(CC) $(CFLAGS) -I include -I test/include $< -c -o $@

executable: $(OBJS) $(MAIN) Makefile
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) $(MAIN) -I include -o bin/connect4

unittest: $(OBJS) $(TESTOBJ) $(TESTMAIN) Makefile
	$(CC) $(CFLAGS) $(TESTOBJ) $(OBJS) $(TESTMAIN) -lcunit -I include -I test/include -o bin/test/test

.PHONY: clean test doxygen
clean:
	rm -rf bin/*.o bin/connect4 bin/test/*.o bin/test/test

test:
	./bin/test/test

doxygen:
	doxygen ./doc/doxygen_config.txt
