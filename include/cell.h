#ifndef CELL_H
#define CELL_H

#include <stdlib.h>
#include <utils.h>
#include "line.h"

/**
 *   \brief Represents each cell of the board. 
 */
typedef struct Cell {
    linkedList * lines; /** List to all the lines the cell belongs */
    state_t state;  /** Current state of the Cell */
} Cell;


/** \brief Cell Constructor
 *  \memberof Cell
 */
Cell* newCell();

/** \brief Cell Destructor
 *  \memberof Cell
 */
void deleteCell(Cell* cell);

/** \brief  Adds a new line reference to the cell.
 *  \memberof Cell
 *  \param line
 */
void addLine(Cell* cell, Line * line);

/** \brief Modifies the cell state and all the lines scores approprietly.
 *  \memberof Cell
 *  \param player New state of the cell.
 */
void setState(Cell* cell, state_t player);

/** \brief Cell State Getter
 *  \memberof Cell
 *  \return State of the Cell
 */
state_t getState(Cell* cell);

#endif
