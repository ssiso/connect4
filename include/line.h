#ifndef LINES_H
#define LINES_H
#include <stdlib.h>

/** \brief Models a 4 cell line of the Board that can be used to
 *  connect the pieces and win the game. The line has an score that
 *  goes from [-4 to 4]. A Player 1 piece adds 1, a player 2 pices adds -1.
 */
typedef struct Line{
   /* Score of the line depending on the pieces it contains*/
   int score; 
}Line;


/** \brief Line constructor
 *  \memberof Line
 *  \param initial_score Initial value of the score attribute
 */
Line * newLine(int initial_score);

/** \brief Line destructor
 *  \memberof Line
 */
void deleteLine(Line * line);

/** \brief Line Score Getter
 *  \memberof Line
 */
int getLineScore(Line * line);

#endif
