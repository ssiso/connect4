#ifndef AI_H
#define AI_H

#include "game.h"




/** \brief AI algorithm to choose the movement the computer should do next. This
	specific function computes the MinMax algorithm with a depth of 4.
 *  \memberof Game
 *  \return Index number of the column the AI algorithms choose to move next.
 */
int getReasonedMove1(Game * game);

#endif

