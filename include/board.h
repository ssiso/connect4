#ifndef BOARD_H
#define BOARD_H
#include <stdlib.h>
#include <stdio.h>
#include "cell.h"

/**
 * \brief Represents the Board of the game.
 */
typedef struct Board{
	/** 2D grid of all the cells in the board */
    Cell*** grid;
    /** Array with the number of empty cells in each column. */
    int * heights;
    /** Number of columns the board has */
    int cols;
    /** Number of rows the board has */ 
    int rows; 
}Board ;


/** \brief Board constructor.
 *  \memberof Board
 *  \param a number of columns of the new board.
 *  \param b number of rows of the new board.
 */
Board * newBoard(int a, int b);

/** \brief Board destructor.
 *  \memberof Board
 */
void deleteBoard(Board* board);


/** \brief Rows Getter.
 *  \memberof Board
 *  \return Number of rows the board has.
 */
int getRows(Board* board);

/** \brief Columns Getter.
 *  \memberof Board
 *  \return Number of columns the board has.
 */
int getCols(Board* board);

/** \brief Cell number Getter.
 *  \memberof Board
 *  \return Number of cells the board has.
 */
int getCells(Board* board);

/** \brief Retrieve a cell reference given their coordinates.
 *  \memberof Board
 *  \param col column coordinate of the cell to retreive.
 *  \param row row coordinate of the cell to retreive.
 *  \return reference to a cell
 */
Cell* getCell(Board* board, int col, int row);

/** \brief Tells if a given column has available space for a new piece.
 *  \memberof Board
 *  \param col Column 
 *  \return True (!=0) if the column has empty cells, otherwise False(0).
 */
int freeSpaceInColumn(Board* board, int col);

/** \brief Adds a new piece to a certain column.
 *  \memberof Board
 *  \param column Column coordinate where to place the new piece.
 *  \param player Player whom the new piece belongs.
 */
void addPiece(Board* board, int column, state_t player);


/** \brief Remove a piece from a certain column.
 *  \memberof Board
 *  \param column Column coordinate where to remove the piece.
 */
void removePiece(Board * board, int column);

/** \brief Converts the board to a basic type array representation
 *  indexed by [rows][columns]
 *  \memberof Board
 *  \return A char** array representation of the board indexed by
 *  [rows][columns].
 */
char ** boardtoArrayRepresentation(Board * board);

#endif
