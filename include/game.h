#ifndef GAME_H
#define GAME_H
#include <limits.h> 
#include <stdlib.h>
#include "board.h"

#define WIN_ONE 4
#define WIN_TWO -4


/** \brief Game controller, is the entry point to the Connect 4
 *  game domain and has all the necessary method to interact with
 *  it.
 */
struct Game{
	/** Board of the game*/
    Board * board;
    /** Ordered registry of all the moves done (indentified by the
     selected column).*/
    int * moves;
    /** Number of moves done since the start of the game, used as a
    index of the moves array.*/
    int move_counter;
    /** Specifies the next player who moves next.*/
    state_t turn;
    /** Function pointer to the AI algorithm. (Models a subclass)*/
    int (*strategy)(struct Game * cB);
    /** Number of possible lines where to connect 4 consecutive pieces*/
    int num_lines;
    /** Array of refecences to all the possible 4 cells lines*/
    Line ** all_lines;   
};
typedef struct Game Game;

// We need to declare the extern function in order to dereference it.
extern int getReasonedMove1(Game * game);


/** \brief Game constructor.
 *  \memberof Game
 *  \param columns Number of columns of the game board.
 *  \param rows Number of rows of the game board.
 *  \param starting_player Integer 1 if starts player one or integer two
 *  if starts player 2.
 *  \param ai Function pointer to the AI strategy.(Models C OOP subclass).
 *  \return New Game instance.
 */
Game * newGame(int columns, int rows, int starting_player,int (*ai)(Game *));

/** \brief Game destructor.
 *  \memberof Game
 */
void deleteGame(Game * game);


/** \brief Check if it is possible to perform a certain move.
 *  \memberof Game
 *  \param column Index of the column where the move goes.
 *  \return True(!=0) if the move is valid, otherwise False.
 */
int validMove(Game * game, int column);

/** \brief Get the number of valid moves left in the board.
 *  \memberof Game
 *  \return Number of valid moves left.
 */
int validMovesLeft(Game * game);

/** \brief Make a player move with all the necessary updates.
 *  \memberof Game
 *  \param column Index of the column where the new piece goes.
 */
void makeMove(Game * game, int column);

/** \brief Restore the game state previous to the last move done.
 *  \memberof Game
 */
void undoMove(Game * game);

/** \brief Gets the winner of the game.
 *  \memberof Game
 *  \return
 */
int winnerIs(Game * game);

/** \brief Gets whose player is the next to make a move.
 *  \memberof Game
 *  \return State prepresenting the next player.
 */
int turn(Game * game);

/** \brief Get the array representation of the current state of the board
 *  indexed by [row][column] (to simplify sequential print).
 *  \memberof Game
 *  \return char*  array representing the board (indexed by [row][column]).
 */
char ** toArrayRepresentation(Game * game);

/** \brief Abstract function of the AI implementation.
 *  \memberof Game
 *  \return Return the place where the computer choose as its next move.
 */
int getReasonedMove(Game * game);

#endif
