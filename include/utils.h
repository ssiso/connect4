#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

#include "line.h"

/** \brief State Enumeration, can be PLAYER_ONE, PLAYER_TWO or EMPTY.
 */
typedef enum {PLAYER_ONE, PLAYER_TWO, EMPTY} state_t;


/** \brief Simple implementation of a linked list of pointers to lines.
 *  The list starts with a ghost(NULL) node and when elements are added,
 *  this node is moved to the end of the list.
 */
struct linkedList{
	/** Reference to the Line item */
    Line * item;
    /** Reference to the following item of the list */
    struct linkedList * next; 
};
typedef struct linkedList linkedList;


/** \brief LinkedList constructor.
 *  \memberof linkedList
 */
linkedList * newLinkedList();

/** \brief LinkedList destructor.
 *  \memberof linkedList
 */
void deleteLinkedList(linkedList * ll);

/** \brief Add a new Line element at the end of the list.
 *  \memberof linkedList
 *  \param item Line element to be added.
 */
void addItem(linkedList * ll, Line * item);

/** \brief Get the next element in order from the linkedList
 *  \memberof linkedList
 *  \return A new linkedList where the first element is the next one of the current element.
 */
linkedList * nextItem(linkedList * ll);

/** \brief Check if the current element is the last one of the list.
 *  \memberof linkedList
 *  \return True(!=0) if the current element is the last one, otherwise False(0)
 */
int lastItem(linkedList * ll);

#endif
