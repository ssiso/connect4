#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "boardTest.h"
#include "cellTest.h"
#include "gameTest.h"
#include "linesTest.h"
#include "utilsTest.h"



/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another CUnit error code on failure.
 */
int main(){
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) return CU_get_error();


   // BOARD TEST
    pSuite = CU_add_suite("Board Suite", init_boardSuite, clean_boardSuite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "Board constructor function",
                            test_boardconstructor))  || 
       (NULL == CU_add_test(pSuite, "Board boardtoArrayRepresentation function",
                            test_boardtoArrayRepresentation)) ||
       (NULL == CU_add_test(pSuite, "Board getRows function", test_boardgetRows)) ||
       (NULL == CU_add_test(pSuite, "Board getCols function", test_boardgetCols)) ||
       (NULL == CU_add_test(pSuite, "Board getCells function", test_boardgetCells)) ||
       (NULL == CU_add_test(pSuite, "Board getCell function", test_boardgetCell)) ||
       (NULL == CU_add_test(pSuite, "Board freeSpaceInColumn function",
                            test_boardfreeSpaceInColumn)) ||
       (NULL == CU_add_test(pSuite, "Board removePiece function", test_boardremovePiece)) ||
       (NULL == CU_add_test(pSuite, "Board addPiece function", test_boardaddPiece)) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

  // CELL TEST
  pSuite = CU_add_suite("Cell Suite", init_cellSuite, clean_cellSuite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "Cell Constructor function", test_cellconstructor)) ||
       (NULL == CU_add_test(pSuite, "Cell addLine function", test_celladdLine)) ||
       (NULL == CU_add_test(pSuite, "Cell setState function", test_cellsetState)) )
   {

      CU_cleanup_registry();
      return CU_get_error();
   }

  // GAME TEST
  pSuite = CU_add_suite("Game Suite", init_gameSuite, clean_gameSuite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "Game constructor function", test_gameconstructor)) ||
       (NULL == CU_add_test(pSuite, "Game validMove function", test_gamevalidMove)) ||
       (NULL == CU_add_test(pSuite, "Game validMovesLeft function", test_gamevalidMovesLeft)) ||
       (NULL == CU_add_test(pSuite, "Game makeMove function", test_gamemakeMove)) ||
       (NULL == CU_add_test(pSuite, "Game undoMove function", test_gameundoMove)) ||
       (NULL == CU_add_test(pSuite, "Game winnerIs function", test_gamewinnerIs)) ||
       (NULL == CU_add_test(pSuite, "Game getBoardString function", test_gamegetBoardString)) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }


   // LINES TEST
   pSuite = CU_add_suite("Lines Suite", init_linesSuite, clean_linesSuite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   if ((NULL == CU_add_test(pSuite, "Lines constructor function", test_linesconstructor)) ||
       (NULL == CU_add_test(pSuite, "Lines getLineScore function", test_linesgetLineScore)) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }
 
   // UTILS TEST
  pSuite = CU_add_suite("Utils Suite", init_utilsSuite, clean_utilsSuite);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }
   if ((NULL == CU_add_test(pSuite, "Utils state enumeration", test_utilsstateenumeration)) ||
       (NULL == CU_add_test(pSuite, "Lines linkedList constructor function", test_utilslinkedListconstructor)) ||
       (NULL == CU_add_test(pSuite, "Lines linkedList addItem function", test_utilsaddItem)) ||
       (NULL == CU_add_test(pSuite, "Lines linkedList nextItem function", test_utilsnextItem)) ||
       (NULL == CU_add_test(pSuite, "Lines linkedList lastItem function", test_utilslastItem)) ) 
   {
      CU_cleanup_registry();
      return CU_get_error();
   }



   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
