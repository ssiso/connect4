
#include "boardTest.h"


int init_boardSuite(void){
   b = newBoard(7,6);
   return 0;
}

int clean_boardSuite(void){
   deleteBoard(b);
   return 0;
}

void test_boardconstructor(void){
   //Check object and parameters
   CU_ASSERT_PTR_NOT_NULL(b);
   CU_ASSERT_EQUAL(b->cols,7);
   CU_ASSERT_EQUAL(b->rows,6);

   //Check heights is an array initialized to 0
   CU_ASSERT_PTR_NOT_NULL(b->heights);
   CU_ASSERT_EQUAL(b->heights[0],0);
   CU_ASSERT_EQUAL(b->heights[1],0);
   CU_ASSERT_EQUAL(b->heights[2],0);
   CU_ASSERT_EQUAL(b->heights[3],0);
   CU_ASSERT_EQUAL(b->heights[4],0);
   CU_ASSERT_EQUAL(b->heights[5],0);
   CU_ASSERT_EQUAL(b->heights[6],0);

   // Check a grid of rows*col cells is created
   CU_ASSERT_PTR_NOT_NULL(b->grid);
   CU_ASSERT_EQUAL(b->grid[0][0]->state,EMPTY);
   CU_ASSERT_EQUAL(b->grid[6][5]->state,EMPTY);

}

void test_boardtoArrayRepresentation(void){
   addPiece(b, 2, PLAYER_ONE);
   addPiece(b, 5, PLAYER_TWO);
   addPiece(b, 5, PLAYER_ONE);
   addPiece(b, 5, PLAYER_TWO);

   char ** b_rep = boardtoArrayRepresentation(b);
   CU_ASSERT_EQUAL(b_rep[0][0],0);
   CU_ASSERT_EQUAL(b_rep[0][2],1);
   CU_ASSERT_EQUAL(b_rep[0][5],2);
   CU_ASSERT_EQUAL(b_rep[1][5],1);
   CU_ASSERT_EQUAL(b_rep[2][5],2);

}

void test_boardgetRows(void){
   CU_ASSERT_EQUAL(getRows(b),b->rows);
}

void test_boardgetCols(void){
   CU_ASSERT_EQUAL(getCols(b),b->cols);
}

void test_boardgetCells(void){
    CU_ASSERT_EQUAL(getCells(b),6*7);
}

void test_boardgetCell(void){
   //Check if getCells returns the reference to the correct cell
   // ! Not tested in isolation because uses a Cell feature
   b->grid[2][4]->state = PLAYER_ONE;
   b->grid[5][3]->state = PLAYER_TWO;

   CU_ASSERT_EQUAL(getCell(b, 2, 4)->state, PLAYER_ONE);
   CU_ASSERT_EQUAL(getCell(b, 5, 3)->state, PLAYER_TWO);
}

void test_boardfreeSpaceInColumn(void){
   CU_ASSERT_TRUE(freeSpaceInColumn(b, 3));
   // Add 6 pieces and check function
   addPiece(b, 3, PLAYER_ONE);
   addPiece(b, 3, PLAYER_ONE); 
   addPiece(b, 3, PLAYER_ONE); 
   addPiece(b, 3, PLAYER_ONE); 
   addPiece(b, 3, PLAYER_ONE);
   CU_ASSERT_TRUE(freeSpaceInColumn(b, 3)); //Still true
   addPiece(b, 3, PLAYER_ONE);
   
   //Check that removing and adding works properly.
   CU_ASSERT_FALSE(freeSpaceInColumn(b, 3));
   removePiece(b, 3);
   CU_ASSERT_TRUE(freeSpaceInColumn(b, 3)); //True again
   removePiece(b, 3);
   addPiece(b, 3, PLAYER_ONE);
   addPiece(b, 3, PLAYER_ONE);
   CU_ASSERT_FALSE(freeSpaceInColumn(b, 3));

}

void test_boardaddPiece(void){
   // Check that pieces are added properly.
   CU_ASSERT_EQUAL(b->grid[4][0]->state, EMPTY);
   addPiece(b, 4, PLAYER_ONE);
   CU_ASSERT_EQUAL(b->grid[4][0]->state, PLAYER_ONE);
   addPiece(b, 4, PLAYER_TWO);
   CU_ASSERT_EQUAL(b->grid[4][1]->state, PLAYER_TWO);
   CU_ASSERT_EQUAL(b->heights[4], 2);

}

void test_boardremovePiece(void){
   // Check that pieces are removed properly.
   addPiece(b, 4, PLAYER_ONE);
   addPiece(b, 4, PLAYER_TWO);
   CU_ASSERT_EQUAL(b->grid[4][0]->state, PLAYER_ONE);
   CU_ASSERT_EQUAL(b->grid[4][1]->state, PLAYER_TWO);
   removePiece(b, 4);
   CU_ASSERT_EQUAL(b->grid[4][1]->state, EMPTY);
   removePiece(b, 4);
   CU_ASSERT_EQUAL(b->grid[4][0]->state, EMPTY);
   CU_ASSERT_EQUAL(b->heights[4], 0);

}