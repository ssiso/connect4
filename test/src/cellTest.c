
#include "cellTest.h"


int init_cellSuite(void){
   cell = newCell();
   return 0;
}

int clean_cellSuite(void){
   deleteCell(cell);
   return 0;
}

void test_cellconstructor(void){
    // The state should be EMPTY.
    CU_ASSERT_EQUAL(cell->state, EMPTY);
    CU_ASSERT_NOT_EQUAL(cell->state, PLAYER_ONE);
    CU_ASSERT_NOT_EQUAL(cell->state, PLAYER_TWO);

    // There should be a line list with 0 elements.
    CU_ASSERT_PTR_NOT_NULL(cell->lines);
    CU_ASSERT_TRUE(lastItem(cell->lines));
}

void test_celladdLine(void){
    // Add 3 new lines with score equals to possition.
    Line * l1 = newLine(0);
    Line * l2 = newLine(1);
    Line * l3 = newLine(2);

    // Check that the lines list exist.
    int counter = 0;
    linkedList * ll;
    CU_ASSERT_PTR_NOT_NULL(cell->lines);
    ll = cell->lines;

    //Add lines
    addLine(cell, l1);
    addLine(cell, l2);
    addLine(cell, l3);


    //Check that the new lines where created properlly and in the correct order.
    while(!lastItem(ll)){
        CU_ASSERT_EQUAL(ll->item->score, counter);
        counter++;
        ll = ll->next;
    }

    //Check that the number of lines is equal to the added lines.
    CU_ASSERT_EQUAL(counter,3);
}

void test_cellsetState(void){
    //Check that each state is set correctly.
    CU_ASSERT_EQUAL(cell->state, EMPTY);
    setState(cell, PLAYER_ONE);
    CU_ASSERT_EQUAL(cell->state, PLAYER_ONE);
    setState(cell, EMPTY);
    CU_ASSERT_EQUAL(cell->state, EMPTY);

    // Check that each possible state change combination changes the score
    // of the lines approprietly.
    Cell * c2 = newCell();
    Cell * c3 = newCell();
    Line * l1 = newLine(0);
    addLine(c2, l1);
    addLine(c3, l1);
    CU_ASSERT_EQUAL(l1->score, 0);
    setState(c2, PLAYER_TWO);
    CU_ASSERT_EQUAL(l1->score, -1);
    setState(c3, PLAYER_TWO);
    CU_ASSERT_EQUAL(l1->score, -2);
    setState(c3, EMPTY);
    CU_ASSERT_EQUAL(l1->score, -1);
    setState(c3, PLAYER_ONE);
    CU_ASSERT_EQUAL(l1->score, 0);

}

