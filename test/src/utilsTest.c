
#include "utilsTest.h"


int init_utilsSuite(void){
   return 0;
}

int clean_utilsSuite(void){
   return 0;
}

void test_utilsstateenumeration(void){
    // Check that the state_t can keep all the emumeration elements
    // and they are properly comparable.
    state_t s1,s2,s3,s4;
    s1 = PLAYER_ONE;
    s2 = PLAYER_TWO;
    s3 = EMPTY;

    CU_ASSERT_FALSE(s1==s2);
    CU_ASSERT_FALSE(s1==s3);
    CU_ASSERT_FALSE(s2==s3);

    s4 = PLAYER_ONE;
    CU_ASSERT_TRUE(s4==s1);
    s4 = PLAYER_TWO;
    CU_ASSERT_TRUE(s4==s2);
    s4 = EMPTY;
    CU_ASSERT_TRUE(s4==s3);
}

void test_utilslinkedListconstructor(void){
    // Check that the linkedList constructor makes an empty linkedList.
    linkedList * ll = newLinkedList();

    CU_ASSERT_PTR_NOT_NULL(ll);
    CU_ASSERT_PTR_NULL(ll->item);
    CU_ASSERT_PTR_NULL(ll->next);

}

void test_utilsaddItem(void){
    // Add some elements and check if the resulting structure is as expected
    // as well as the elements are placed as expected.
    linkedList * ll = newLinkedList();
    Line * l1 = newLine(1);
    Line * l2 = newLine(2);

    addItem(ll,l1);
    addItem(ll,l2);

    CU_ASSERT_PTR_NOT_NULL(ll);
    CU_ASSERT_PTR_NOT_NULL(ll->item);
    CU_ASSERT_PTR_NOT_NULL(ll->next);
    CU_ASSERT_PTR_NOT_NULL(ll->next->item);
    CU_ASSERT_PTR_NOT_NULL(ll->next->next);
    CU_ASSERT_PTR_NULL(ll->next->next->item);
    CU_ASSERT_PTR_NULL(ll->next->next->next);

    CU_ASSERT_EQUAL(getLineScore(ll->item),1);
    CU_ASSERT_EQUAL(getLineScore(ll->next->item),2);

}

void test_utilsnextItem(void){
    // Check that the next item function really returns a reference to the 
    // next item in the list.
    linkedList * ll = newLinkedList();
    Line * l1 = newLine(1);
    Line * l2 = newLine(2);
    Line * l3 = newLine(3);

    addItem(ll,l1);
    addItem(ll,l2);
    addItem(ll,l3);

   CU_ASSERT_EQUAL(getLineScore(nextItem(ll)->item),2);
   CU_ASSERT_EQUAL(getLineScore(nextItem(nextItem(ll))->item),3);
}

void test_utilslastItem(void){
    //Check that the lastItem function only returns True when it is asked
    //at the final linkedList ghost node.
    linkedList * ll = newLinkedList();
    Line * l1 = newLine(1);
    Line * l2 = newLine(2);
    Line * l3 = newLine(3);

    addItem(ll,l1);
    addItem(ll,l2);
    addItem(ll,l3);

    CU_ASSERT_FALSE(lastItem(ll));
    CU_ASSERT_FALSE(lastItem(ll->next));
    CU_ASSERT_FALSE(lastItem(ll->next->next));
    CU_ASSERT_TRUE(lastItem(ll->next->next->next));

}

