
#include "gameTest.h"


int init_gameSuite(void){
  g = newGame(7, 6, 1, &getReasonedMove1);
  return 0;
}

int clean_gameSuite(void){
  deleteGame(g);
  return 0;
}

void test_gameconstructor(void){
  CU_ASSERT_PTR_NOT_NULL(g);
  
  // Check the game has been properlly initialized
  CU_ASSERT_EQUAL(g->board->cols, 7);
  CU_ASSERT_EQUAL(g->board->rows, 6);
  CU_ASSERT_EQUAL(g->turn, PLAYER_ONE);
  CU_ASSERT_EQUAL(g->move_counter, 0);

  // Invalid preconditions
  CU_ASSERT_PTR_NULL(newGame(3, 7, 1, &getReasonedMove1));
  CU_ASSERT_PTR_NULL(newGame(7, -2, 1, &getReasonedMove1));
  CU_ASSERT_PTR_NULL(newGame(7, 7, 0, &getReasonedMove1));

}

void test_gamevalidMove(void){
  // Initillay all moves available
  CU_ASSERT_TRUE(validMove(g, 0));
  CU_ASSERT_TRUE(validMove(g, 1));
  CU_ASSERT_TRUE(validMove(g, 2));
  CU_ASSERT_TRUE(validMove(g, 3));
  CU_ASSERT_TRUE(validMove(g, 4));
  CU_ASSERT_TRUE(validMove(g, 5));
  CU_ASSERT_TRUE(validMove(g, 6));

  //Check when a move is not available
  makeMove(g, 5);
  makeMove(g, 5);
  makeMove(g, 5);
  makeMove(g, 5);
  makeMove(g, 5);
  CU_ASSERT_TRUE(validMove(g, 5));
  makeMove(g, 5);    
  CU_ASSERT_FALSE(validMove(g, 5));

}

void test_gamevalidMovesLeft(void){

  // Check if initially there are the proper moves left
  Game * g2 = newGame(7, 6, 1, &getReasonedMove1);
  CU_ASSERT_EQUAL(validMovesLeft(g2),6*7);
  
  // Check if it remains coherent after some moves.
  makeMove(g2, 5);
  makeMove(g2, 5);
  makeMove(g2, 5);
  CU_ASSERT_EQUAL(validMovesLeft(g2),(6*7)-3);

}

void test_gamemakeMove(void){
  int prev_moves = g->move_counter;
  int prev_height = g->board->heights[2];
  int prev_turn = g->turn;

  makeMove(g, 2);
  CU_ASSERT_EQUAL(g->move_counter, prev_moves + 1);
  CU_ASSERT_EQUAL(g->moves[g->move_counter], 2);
  CU_ASSERT_EQUAL(g->board->heights[2], prev_height + 1);
  CU_ASSERT_NOT_EQUAL(g->turn, prev_turn);

}

void test_gameundoMove(void){

  makeMove(g, 6);

  // Check if undoMove makes the proper changes.
  int prev_moves = g->move_counter;
  int prev_height = g->board->heights[6];
  int prev_turn = g->turn;
  
  undoMove(g);
  CU_ASSERT_EQUAL(g->move_counter, prev_moves - 1);
  CU_ASSERT_EQUAL(g->board->heights[6], prev_height - 1);
  CU_ASSERT_NOT_EQUAL(g->turn, prev_turn);

}

void test_gamewinnerIs(void){
  
  // Generate finished game
  Game * g3 = newGame(7, 6, 1, &getReasonedMove1);
  CU_ASSERT_EQUAL(winnerIs(g3), 0);
  makeMove(g3, 1);
  makeMove(g3, 1);
  makeMove(g3, 2);
  makeMove(g3, 2);
  makeMove(g3, 3);
  makeMove(g3, 3);
  CU_ASSERT_EQUAL(winnerIs(g3), 0);
  makeMove(g3, 4);
  CU_ASSERT_EQUAL(winnerIs(g3), 1);

}

void test_gamegetBoardString(void){
  Game * g4 = newGame(7, 6, 1, &getReasonedMove1);
  makeMove(g4, 1); // Player 1 moves to 1
  makeMove(g4, 3); // Player 2 moves to 3
  makeMove(g4, 5); // Player 1 moves to 5
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[0][0],0);
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[1][1],0);
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[1][3],0);
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[0][1],1);
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[0][3],2);
  CU_ASSERT_EQUAL(toArrayRepresentation(g4)[0][5],1);

}


