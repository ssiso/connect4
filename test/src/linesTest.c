
#include "linesTest.h"


int init_linesSuite(void){
   l = newLine(0);
   return 0;
}

int clean_linesSuite(void){
   deleteLine(l);
   return 0;
}

void test_linesconstructor(void){
    // Check the the line reference contains an initialized line.
    CU_ASSERT_PTR_NOT_NULL(l);
    CU_ASSERT_EQUAL(l->score,0);
}

void test_linesgetLineScore(void){
    CU_ASSERT_EQUAL(getLineScore(l),0);
    l->score=1;
    CU_ASSERT_EQUAL(getLineScore(l),1);
}

