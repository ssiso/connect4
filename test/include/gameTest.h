
#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "game.h"
#include "aiStrategy.h"


Game * g;

int init_gameSuite(void);
int clean_gameSuite(void);

void test_gameconstructor(void);
void test_gamevalidMove(void);
void test_gamevalidMovesLeft(void);
void test_gamemakeMove(void);
void test_gameundoMove(void);
void test_gamewinnerIs(void);
void test_gamegetBoardString(void);

