
#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "utils.h"



int init_utilsSuite(void);
int clean_utilsSuite(void);

void test_utilsstateenumeration(void);
void test_utilslinkedListconstructor(void);
void test_utilslinkedListdestructor(void);
void test_utilsaddItem(void);
void test_utilsnextItem(void);
void test_utilslastItem(void);
