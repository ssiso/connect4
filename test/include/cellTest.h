
#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "cell.h"


Cell * cell;

int init_cellSuite(void);
int clean_cellSuite(void);
void test_cellconstructor(void);
void test_celladdLine(void);
void test_cellsetState(void);

