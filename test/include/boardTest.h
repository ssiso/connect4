
#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"

#include "board.h"


Board * b;

int init_boardSuite(void);
int clean_boardSuite(void);

void test_boardconstructor(void);
void test_boardtoArrayRepresentation(void);
void test_boardgetRows(void);
void test_boardgetCols(void);
void test_boardgetCells(void);
void test_boardgetCell(void);
void test_boardfreeSpaceInColumn(void);
void test_boardaddPiece(void);
void test_boardremovePiece(void);
